/** 
  * 1. Create variables for firstName, lastName, streetAddress, city, state, and zipcode. 
  * Use this information to create a formatted address block that could be printed onto an envelope.
  */

const firstName = 'Patricia';
const lastName = 'Marcheselli';
const streetAddress = '123 ' + 'First Ave';
const city = 'Seattle';
const state = 'WA';
const zipCode = '98101';

myAddress = firstName + ' ' + lastName + '\n' + streetAddress + '\n' + city + ', ' + state + ', ' + zipCode; 


/** 
  * 2. You are given a string in this format: 
  * firstName lastName (assume no spaces in either)
  * streetAddress
  * city, state zip (could be spaces in city and state)
  * Write code that is able to extract this full string into each variable. 
  * Hint: use indexOf, slice, and/or substring
*/

myAddress.indexOf(firstName);
// output: 0
firstName.length;
// output: 8
myAddress.slice(0,8);
// output: "Patricia"
myAddress.indexOf(lastName);
// output: 9;
lastName.length;
// output: 11;
myAddress.slice(9,20);
// output: "Marcheselli"
myAddress.indexOf(streetAddress);
// output: 21
streetAddress.length;
// output: 13
myAddress.slice(21,34);
// output: "123 First Ave"
myAddress.indexOf(city);
// output: 35
city.length;
// output: 7
myAddress.slice(35,42);
// output: "Seattle"
myAddress.indexOf(state);
// output: 44
state.length;
// output: 2
myAddress.slice(44,46);
// output: "WA"
myAddress.indexOf(zipCode);
// output: 48
zipCode.length;
// output: 5
myAddress.slice(48,53);
// output: "98101"

