/** 
  * Find the middle date and time between the following two dates:
  * 1/1/2019 00:00:00 and 4/1/2019 00:00:00
  */

const startDate = new Date(2019,0,1);
const endDate = new Date(2019,3,1);

// Get the amount of time between both dates
const difference = endDate.getTime() - startDate.getTime();
console.log(difference); 
// output: 7772400000 

// Divide amount of time between both dates by 2 to get the midpoint
const midpoint = new Date(startDate.getTime() + difference / 2);
console.log(midpoint);
// output: Thu Feb 14 2019 23:30:00 GMT-0800 (Pacific Standard Time)

// Second solution (a simpler way):
// Divide amount of time between both dates by 2 to get the midpoint
const midpoint = new Date((startDate.getTime() + endDate.getTime()) / 2);
// output: Thu Feb 14 2019 23:30:00 GMT-0800 (Pacific Standard Time)