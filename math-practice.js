/**
  * 1. Using the Math object, put together a code snippet that allows you to
  * draw a random card with a value between 1 and 13
  */

// Increment + 1 is used so as not to draw card with value of 0 
function drawRandom() {
  return Math.floor((Math.random() * 13) + 1);
 }  
// Pass a max value of 13 and get random number 
console.log(drawRandom(13));
   
// 2. Draw 3 cards and use Math to determine the highest card
let drawCard = [ card1, card2, card3];
console.log(Math.max(...drawCard));

// 3. What is surface area of pizza?
// To find surface area of a circle, use Pi x radius squared 
const pizzaDiameter1 = 13;
const pizzaDiameter2 = 17;
   
surfaceArea1 = Math.PI * (pizzaDiameter1 / 2) ** 2; 
// output = 132.73228961416876
surfaceArea2 = Math.PI * (pizzaDiameter2 / 2) ** 2; 
// output = 226.98006922186255
   
// 4. What is the cost per square inch of each pizza?
const price1 = 16.99;
const price2 = 19.99;
   
cpsi1 = price1 / pizzaDiameter1; 
// output = 1.3069230769230769
//use .toFixed to end a long number
console.log(cpsi1.toFixed(2));
// output: 1.31
cpsi2 = price2 / pizzaDiameter2; 
// output = 1.1758823529411764
//use .toFixed to end a long number 
console.log(cpsi2.toFixed(2));
// output: 1.18